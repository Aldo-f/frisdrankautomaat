<?php

use Frisdrank\Business\FrisdrankService;
use Frisdrank\Business\GeldladeService;

// global vars
$msgError["msg"] = [];
$msgSuccess["msg"] = [];

/**
 * Funtioneel
 */
require "vendor/autoload.php";
require "src/twig.php";
require "src/assets/functionality/ingelogd.php";
require "src/assets/functionality/geworpenMunten.php";
require "src/assets/functionality/koopFrisdrank.php";
// require "src/assets/functionality/koopFrisdrank2.php";

/**
 * Visueel
 */
require "src/presentation/layout_parts/header.php";
require "src/presentation/layout_parts/alerts.php";

// munten
$geldladeSvc = new GeldladeService();
$geldladeLijst = $geldladeSvc->getAll();

$view = $twig->render(
    "BetaalKnoppen.twig",
    array("geldladeLijst" => $geldladeLijst)
);
print($view);

echo '<div class="ui divider hidden"></div>';

// frisdranken
$frisdrankSvc = new FrisdrankService();
$frisdrankLijst = $frisdrankSvc->getAllFrisdrank();

$view = $twig->render(
    "FrisdrankOverzicht.twig",
    array("frisdrankLijst" => $frisdrankLijst,  "ingelogd" => $ingelogd),

);
print($view);
?>


<?php

require "src/presentation/layout_parts/footer.php";