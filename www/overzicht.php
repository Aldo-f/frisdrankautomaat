<?php
use Frisdrank\Business\FrisdrankService;
use Frisdrank\Business\GeldladeService;

// global vars
$msgError["msg"] = [];
$msgSuccess["msg"] = [];

/**
 * Funtioneel
 */
require "vendor/autoload.php";
require "src/twig.php";
require "src/assets/functionality/ingelogd.php";
require "src/assets/functionality/pasMuntenAan.php";
require "src/assets/functionality/pasFrisdrankAan.php";

/**
 * Visueel
 */
require "src/presentation/layout_parts/header.php";
require "src/presentation/layout_parts/alerts.php";


// munten
$geldladeSvc = new GeldladeService();
$geldladeLijst = $geldladeSvc->getAll();

$view = $twig->render(
    "GeldladeOverzicht.twig",
    array("geldladeLijst" => $geldladeLijst, "ingelogd" => $ingelogd)
);
print($view);

echo '<div class="ui divider hidden"></div>';

// frisdranken
$frisdrankSvc = new FrisdrankService();
$frisdrankLijst = $frisdrankSvc->getAllFrisdrank();

$view = $twig->render(
    "frisdrankOverzicht.twig",
    array("frisdrankLijst" => $frisdrankLijst,  "ingelogd" => $ingelogd),

);
print($view);


?>


<?php
require "src/presentation/layout_parts/footer.php";