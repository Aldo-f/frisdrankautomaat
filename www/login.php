<?php
// global vars
$msgError["msg"] = [];
$msgSuccess["msg"] = [];

/**
 * Funtioneel
 */
require "vendor/autoload.php";
require "src/twig.php";
require "src/assets/functionality/ingelogd.php";
require "src/assets/functionality/controleerLogin.php";

/**
 * Visueel
 */
require "src/presentation/layout_parts/header.php";
require "src/presentation/layout_parts/alerts.php";

$view = $twig->render(
    "loginForm.twig",
    array("gebruikerLijst" => array("admin", "password"))
);
print($view);

// footer
require "src/presentation/layout_parts/footer.php";