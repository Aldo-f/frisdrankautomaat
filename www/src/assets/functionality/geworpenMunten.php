<?php
use Frisdrank\Business\GeldladeService;

// session_start();

if (isset($_COOKIE["totaal"])) {
    $totaal = $_COOKIE["totaal"];
} else {
    $totaal = 0;
}

if (isset($_GET["actie"]) && $_GET["actie"] == "werpmunt") {


    // controleer geworpen munt
    $valid = true;
    if (isset($_GET["muntId"])) {
        $muntId = $_GET["muntId"];
    } else {
        $valid = false;
    }
    if (isset($_GET["munt"])) {
        $munt = $_GET["munt"];
    } else {
        $valid = false;
    }

    // werp munt in DB
    if ($valid) {
        // echo "Munt geworpen";

        $geldladeSvc = new GeldladeService();
        // Standaard 1 munt per worp
        $geldlade = $geldladeSvc->add($muntId, 1);

        // controleer ingeworpen munt
        if ($geldlade) {
            $msgSuccess["header"] = "Je wierp een munt";
            array_push($msgSuccess["msg"], "Een stuk van €$munt werd toegevoegd.");

            // Hou totaal bij in var an in cookie
            $totaal += $geldlade->getMunt();
            setcookie("totaal", $totaal, time() + 60 * 60 * 24 * 30); // 30 dagen

            $totaal = number_format($totaal, 2);
            $msg = "Het totaal is: € $totaal";
            array_push($msgSuccess["msg"], $msg);
        } else {
            $msgError["header"] = "De ingeworpen munt werd niet herkent";
            array_push($msgSuccess["msg"], "Er is een totaal van € $totaal beschikbaar");
        }
    } else {
        $msgError["header"] = "De munt werd niet goed ingebracht";
        array_push($msgSuccess["msg"], "Er is een totaal van € $totaal beschikbaar");
    }
}