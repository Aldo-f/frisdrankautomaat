<?php
// $ingelogd = true;

// start session
session_start();

// Controleer of gebruiker niet op 'logout' klikte
if (isset($_GET['actie']) && $_GET['actie'] == "logout") {
    // unset Gebruiker
    unset($_SESSION['Gebruiker']);
    $ingelogd = false;

    $msgSuccess["header"] = "Successvol uitgelog";
    array_push($msgSuccess["msg"], "U hebt zich successvol uitgelogd");
} else {
    // Indien reeds ingelogd?
    if (isset($_SESSION['Gebruiker'])) {
        $gebruiker = unserialize($_SESSION['Gebruiker']);

        // DEBUG
        // echo $gebruiker->getNaam();

        $ingelogd = true;
    } else {
        // niet ingelogd
        $ingelogd = false;
    }
}