<?php
use Frisdrank\Business\GebruikerService;

if (!$ingelogd) {
    if (isset($_POST["actie"]) && $_POST["actie"] == "login") {
        if (isset($_POST["username"]) && isset($_POST["password"])) {

            $username = $_POST["username"];
            $password = $_POST["password"];

            // DEBUG
            // $msgSuccess["header"] = "Dit gaf je in";
            // array_push($msgSuccess["msg"], "username: $username");
            // array_push($msgSuccess["msg"], "password: $password");

            $valid = true;
            // Valideer input


            if ($valid) {
                // Controleer gegevens

                $gebruikerSvc = new GebruikerService();
                $resultaat = $gebruikerSvc->getByLogin($username, $password);

                if ($resultaat == false) {
                    // verkeerde login
                    $msgError["header"] = "Verkeerde login";
                    array_push($msgError["msg"], "Je kan niet inloggen met opgegeven gegevens");
                } else {
                    // Alleen voor naamsverduidelijking
                    $gebruiker = $resultaat[0];

                    $id = $gebruiker->getId();
                    $naam = $gebruiker->getNaam();
                    $wachtwoord = $gebruiker->getWachtwoord();

                    $msgSuccess["header"] = "Welkom $naam";
                    array_push($msgSuccess["msg"], "je bent successvol ingelogd");

                    // // Plaats gebruiker in session vars
                    $_SESSION["Gebruiker"] = serialize($gebruiker);

                    // // Plaats email in cookie vars ( voor 1 week )
                    // setcookie("email", $gebruikerEmail, time() + 3600 * 24 * 7);

                    // Voor de navigatie
                    $ingelogd = true;
                }
            }
        }
    }
}