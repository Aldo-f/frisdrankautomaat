<?php
use Frisdrank\Business\FrisdrankService;
use Frisdrank\Business\GeldladeService;

if (isset($_GET["actie"]) && $_GET["actie"] == "koop") {
    // controleer geworpen munt
    $valid = true;
    if (isset($_GET["frisdrank"])) {
        $frisdrankId = $_GET["frisdrank"];
    } else {
        $valid = false;
        $msgError["header"] = "Welke frisdrank koos je? ";

        array_push($msgError["msg"], "Geen frisdrank id meegegeven");
    }

    // haal prijs uit DB 
    $frisdrankSvc = new FrisdrankService;
    $frisdrank = $frisdrankSvc->getById($frisdrankId);
    $prijs = $frisdrank->getPrijs();
    $naam = $frisdrank->getNaam();


    // Controleer
    if ($totaal >= $prijs) {
        $msgSuccess["header"] = "Je hebt genoeg geld";
        $msgSuccess["msg"] = ["Je koos $naam"];
    } else {
        $valid = false;
        $msgError["header"] = "Onvoldoende geld beschikbaar";
        $totaal = number_format($totaal, 2);
        // $msgError["msg"] = ["Er is een totaal van € $totaal beschikbaar"];

        array_push($msgError["msg"], "Er is een totaal van € $totaal beschikbaar");
    }

    // DB
    if ($valid) {
        $frisdrankUitDB = $frisdrankSvc->remove($frisdrankId, 1);

        if ($frisdrankUitDB) {
            // $msgSuccess["header"] = "Je wierp een munt";
            $naam = $frisdrankUitDB->getNaam();

            $msgSuccess["header"] = "Aankoop was een success";
            $msgSuccess["msg"] = array("Je kreeg: $naam");

            $prijs = $frisdrankUitDB->getPrijs();

            /// Geef geld terug
            // Bereken hoveel je terug moet hebben
            // $totaal  - prijs 
            $verschil = $totaal - $prijs;

            // $msgSuccess["msg"] = ["Je krijgt nog $verschil terug"];
            $verschil = number_format($verschil, 2);
            array_push($msgSuccess["msg"], "Je krijgt nog €$verschil terug");


            /// Haal de munten uit DB
            // Bekijk welke munten er zijn            
            $geldladeSvc = new GeldladeService();
            $geldlade = $geldladeSvc->getAll();

            foreach ($geldlade as $munt) {
                // $verschil = $verschil; // (stond hier nog voor cast naar float)
                $waarde = $munt->getMunt();
                $aantal = (int)$munt->getAantal();
                $id = $munt->getId();

                $i = 0;
                // terwijl verschil >= de waarde van de huidige munt en wanneer de munt aanwezig is
                while ($verschil >= $waarde && $aantal > 0) {
                    $i++;
                    $verschil = $verschil - $waarde;

                    //FIX: Pas float aan naar 2 decimals -> want 0.2 != 0.20 
                    // (de eerste is een float, en werd berekend van 0.4 - 0.2 != 0.2 maar wel ≈ 0.2)
                    // door de ≈ 0.2 aan te passen naar = 0.2 klopt de "$verschil >= \$waarde" wel
                    $verschil = number_format($verschil, 1);

                    // Controle:
                    // $verschil >= $waarde ? $b = "true" : $b = "false";
                    // array_push($msgSuccess["msg"], "\$verschil: $verschil, \$waarde: $waarde dus \$verschil >= \$waarde: $b");

                    // remove $munt
                    $geldladeSvc->remove($id, 1);
                    $aantal--;
                }
                // Toon wat je terug kreeg
                $waarde = number_format($waarde, 2);
                $i > 0 ? array_push($msgSuccess["msg"], "$i x €$waarde") : "";

                if ($aantal <= 0) {
                    $msgError["header"] = "Munt niet meer aanwezig";
                    array_push($msgError["msg"], "Er zijn geen stukjes meer van €$waarde aanwezig");
                }
            }

            $verschil = number_format($verschil, 2);

            // Toon enkel de error als er nog geld tegoed is 
            if ($verschil > 0) {
                $msgError["header"] = 'Onvoldoende munten aanwezig';
                array_push($msgError["msg"], "Nog €$verschil in automaat ");
                array_push($msgError["msg"], "Dit bedrag is te gebruiken bij je volgende aankoop");
            } else {
                $msgError["header"] = null;
                $msgError["msg"] = null;
                array_push($msgSuccess["msg"], "Je kreeg gepast terug");
            }

            // Pas het totaal bedrag aan in cookie
            setcookie("totaal", $verschil, time() + 60 * 60 * 24 * 30); // 30 dagen
        } else {

            $msgError["header"] = "Sorry";
            array_push($msgError["msg"], "$naam is niet meer aanwzig");
        }
    }
}