<?php
use Frisdrank\Business\FrisdrankService;
use Frisdrank\Business\GeldladeService;

if (isset($_GET["actie"]) && $_GET["actie"] == "koop") {
    // controleer geworpen munt
    $valid = true;
    if (isset($_GET["frisdrank"])) {
        $frisdrankId = $_GET["frisdrank"];
    } else {
        $valid = false;
        $msgError["header"] = "Welke frisdrank koos je? ";

        array_push($msgError["msg"], "Geen frisdrank id meegegeven");
    }

    // haal prijs uit DB 
    $frisdrankSvc = new FrisdrankService;
    $frisdrank = $frisdrankSvc->getById($frisdrankId);
    $prijs = $frisdrank->getPrijs();
    $naam = $frisdrank->getNaam();


    // Controleer
    if ($totaal >= $prijs) {
        $msgSuccess["header"] = "Je hebt genoeg geld";
        $msgSuccess["msg"] = ["Je koos $naam"];
    } else {
        $valid = false;
        $msgError["header"] = "Onvoldoende geld beschikbaar";
        $totaal = number_format($totaal, 2);
        // $msgError["msg"] = ["Er is een totaal van € $totaal beschikbaar"];

        array_push($msgError["msg"], "Er is een totaal van € $totaal beschikbaar");
    }

    // DB
    if ($valid) {
        $frisdrankUitDB = $frisdrankSvc->remove($frisdrankId, 1);

        if ($frisdrankUitDB) {
            // $msgSuccess["header"] = "Je wierp een munt";
            $naam = $frisdrankUitDB->getNaam();

            $msgSuccess["header"] = "Aankoop was een success";
            $msgSuccess["msg"] = array("Je kreeg: $naam");

            $prijs = $frisdrankUitDB->getPrijs();

            /// Geef geld terug
            // Bereken hoveel je terug moet hebben
            // $totaal  - prijs 
            $verschil = $totaal - $prijs;

            // $msgSuccess["msg"] = ["Je krijgt nog $verschil terug"];
            $verschil = number_format($verschil, 2);
            array_push($msgSuccess["msg"], "Je krijgt nog €$verschil terug");


            /// Haal de munten uit DB
            // Bekijk welke munten er zijn            
            $geldladeSvc = new GeldladeService();
            $geldlade = $geldladeSvc->getAll();

            // while ($verschil > 0) {
            foreach ($geldlade as $munt) {
//=Adinda= onderstaande instructie, $verschil = $verschil; ?                
                //$verschil = $verschil;
                $waarde = $munt->getMunt();
                $aantal = (int)$munt->getAantal();
                $id = $munt->getId();

                $i = 0;
                // terwijl verschil >= de waarde van de huidige munt en wanneer de munt aanwezig is
//=Adinda= extra haken nodig?                
                //while ((float)$verschil >= (float)$waarde && $aantal > 0) {
                while (((float)$verschil >= (float)$waarde) && $aantal > 0) {
                    $i++;
                    $verschil = (float)$verschil - (float)$waarde;
//=Adinda= hieronder toegevoegd:                    
$verschil = number_format($verschil, 2);                    
                    // remove $munt
                    $geldladeSvc->remove($id, 1);
                    $aantal--;
                }
                // Toon wat je terug kreeg
                $waarde = number_format($waarde, 2);
                $i > 0 ? array_push($msgSuccess["msg"], "$i x €$waarde") : "";
//=Adinda hieronder toegevoegd:                
$ber = number_format($munt->getMunt()*((int)$munt->getAantal()-$i),2);                
            }
            // }
//=Adinda= hieronder in commentaar gezet:
//$verschil = number_format($verschil, 2);
//=Adinda= hieronder gewijzigd:            
            //array_push($msgSuccess["msg"], "Nog €$verschil in automaat. ");
            array_push($msgSuccess["msg"], "Nog €$ber in automaat. ");

            // Pas het totaal bedrag aan in cookie
            // setcookie("test", "test");
            setcookie("totaal", $verschil, time() + 60 * 60 * 24 * 30); // 30 dagen
        } else {

            $msgError["header"] = "Sorry";
            array_push($msgError["msg"], "$naam is niet meer aanwzig");
        }


        // controleer ingeworpen munt
        // if ($geldlade) {
        //     $msgSuccess["header"] = "Je wierp een munt";
        //     $msgSuccess["msg"] = ["je wierp € $munt"];

        //     // Hou totaal bij in var an in cookie
        //     $totaal += $geldlade->getMunt();
        //     setcookie("totaal", $totaal, mktime() . time() + 60 * 60 * 24 * 30); // 30 dagen


        //     $totaal = number_format($totaal, 2);
        //     $msg = "Het totaal is: € $totaal";
        //     array_push($msgSuccess["msg"], $msg);
        // } else {
        //     $msgError["header"] = "De ingeworpen mutn werd niet herkent";
        //     $msgError["msg"] = ["Er is een totaal van € $totaal beschikbaar"];
        // }
    } else { }
}