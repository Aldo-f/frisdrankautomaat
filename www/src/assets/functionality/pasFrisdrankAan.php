<?php
use Frisdrank\Business\FrisdrankService;

if ($ingelogd) {
    if (isset($_GET["actie"]) && $_GET["actie"] == "pasFrisdrankAan") {

        // max value
        $value = 20;

        $frisdrankSvc = new FrisdrankService;
        $frisdrank = $frisdrankSvc->set($value);
        $msgSuccess["header"] = "Succesvol aangepast";
        array_push($msgSuccess["msg"], "De automaat werd aangevuld");
    }
} else {
    $msgError["header"] = "Niet ingelogd";
    array_push($msgError["msg"], "Je moet je inloggen om de automaat te kunnen aanvullen");
}