<?php

use Frisdrank\Business\GeldladeService;

if ($ingelogd) {
    if (isset($_POST["actie"]) && $_POST["actie"] == "pasMuntenAan") {
        // Haal vanales op^
        // Haal eventueel zaken op
        // aantal POST zonder actie:
        foreach ($_POST as $key => $value) {
            if ($key != "actie") {

                // echo "Field " . htmlspecialchars($key) . " is " . htmlspecialchars($value) . "<br>";

                // TODO: controleer hier nog elke value
                $valid = true;

                // pas elke waarde aan
                if ($valid) {
                    $geldladeSvc = new GeldladeService;
                    $munt = $geldladeSvc->set($key, $value);
                    $naam = $munt->getMunt();
                    $aantal = $munt->getAantal();
                    array_push($msgSuccess["msg"], "$aantal x €$naam");
                }
            }
        }
        $msgSuccess["header"] = "Succesvol aangepast";
        array_push($msgSuccess["msg"], "De aanpassingen werden successvol verwerkt");
    }
} else {
    // Niet ingelogd
}