<?php
namespace Frisdrank\Business;

use Frisdrank\Data\GeldladeDAO;

class GeldladeService
{

    public function getAll()
    {

        $geldladeDAO = new GeldladeDAO();
        $lijst = $geldladeDAO->getAll();
        return $lijst;
    }

    public function add($id, $aantal)
    {
        $geldladeDAO = new GeldladeDAO();
        $munt = $geldladeDAO->add($id, $aantal);
        return $munt;
    }
    public function remove($id, $aantal)
    {
        $geldladeDAO = new GeldladeDAO();
        $lijst = $geldladeDAO->remove($id, $aantal);
        return $lijst;
    }
    public function set($id, $aantal)
    {
        $geldladeDAO = new GeldladeDAO();
        $lijst = $geldladeDAO->set($id, $aantal);
        return $lijst;
    }
}