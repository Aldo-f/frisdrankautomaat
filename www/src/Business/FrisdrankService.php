<?php
namespace Frisdrank\Business;

use Frisdrank\Data\FrisdrankDAO;


class FrisdrankService
{

    public function getAllFrisdrank()
    {
        $frisdrankDAO = new FrisdrankDAO();
        $lijst = $frisdrankDAO->getAll();
        return $lijst;
    }

    public function getById($id)
    {
        $frisdrankDAO = new FrisdrankDAO();
        $lijst = $frisdrankDAO->getById($id);
        return $lijst;
    }

    public function remove($id, $aantal)
    {
        $frisdrankDAO = new FrisdrankDAO();
        $lijst = $frisdrankDAO->remove($id, $aantal);
        return $lijst;
    }

    public function add($id, $aantal)
    {
        $frisdrankDAO = new FrisdrankDAO();
        $lijst = $frisdrankDAO->add($id, $aantal);
        return $lijst;
    }
    public function set($aantal)
    {
        $frisdrankDAO = new FrisdrankDAO();
        $lijst = $frisdrankDAO->set($aantal);
        return $lijst;
    }
}