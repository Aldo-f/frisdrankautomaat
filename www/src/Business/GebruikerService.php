<?php
namespace Frisdrank\Business;

use Frisdrank\Data\GebruikerDAO;

class GebruikerService
{
    public function getById($id)
    {
        $gebruikerDAO = new GebruikerDAO();
        $lijst = $gebruikerDAO->getById($id);
        return $lijst;
    }

    public function getByLogin($username, $password)
    {
        $gebruikerDAO = new GebruikerDAO();
        $lijst = $gebruikerDAO->getByLogin($username, $password);
        return $lijst;
    }
}