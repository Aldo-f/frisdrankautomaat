<?php
namespace Frisdrank\Data;

use PDO;
use Frisdrank\Data\DBconfig;
use Frisdrank\Entities\Gebruiker;

class GebruikerDAO
{
    public function getByLogin($username, $password)
    {
        $dbh = new PDO(
            DBconfig::$DB_CONNSTRING,
            DBconfig::$DB_USERNAME,
            DBconfig::$DB_PASSWORD
        );

        $sql =
            "SELECT id, naam, wachtwoord
            FROM aldofi1q_opleiding_frisdrank.gebruiker
            WHERE naam = :naam AND wachtwoord = :wachtwoord";
        $stmt = $dbh->prepare($sql);
        $stmt->execute(array(
            ':naam' => $username,
            ':wachtwoord' => $password
        ));

        $resultSet = $stmt->fetch(PDO::FETCH_ASSOC);

        $gebruikers = array();
        if ($resultSet) {
            $klant = new Gebruiker(
                $resultSet["id"],
                $resultSet["naam"],
                $resultSet["wachtwoord"]
            );
            array_push($gebruikers, $klant);

            $dbh = null;
            return $gebruikers;
        } else {
            $dbh = null;
            return false;
        }
    }
}
