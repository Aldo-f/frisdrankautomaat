<?php
namespace Frisdrank\Data;

use PDO;
use Frisdrank\Data\DBconfig;
use Frisdrank\Entities\Geldlade;

class GeldladeDAO
{
    public function getAll()
    {
        $sql =
            "SELECT id, munt, aantal
            FROM aldofi1q_opleiding_frisdrank.geldlade
            order by munt desc;";
        $dbh = new PDO(
            DBconfig::$DB_CONNSTRING,
            DBconfig::$DB_USERNAME,
            DBconfig::$DB_PASSWORD
        );
        $resultSet = $dbh->query($sql);

        $lijst = array();
        foreach ($resultSet as $rij) {
            $item = Geldlade::create(
                $rij["id"],
                $rij["munt"],
                $rij['aantal']
            );
            array_push($lijst, $item);
        }
        $dbh = null;
        // echo $lijst;
        return $lijst;
    }
    public function getById($id)
    {
        $sql =
            "SELECT * 
            FROM aldofi1q_opleiding_frisdrank.geldlade
            WHERE id = :id;";
        $dbh = new PDO(
            DBconfig::$DB_CONNSTRING,
            DBconfig::$DB_USERNAME,
            DBconfig::$DB_PASSWORD
        );

        $stmt = $dbh->prepare($sql);
        $stmt->execute(array(
            ':id' => $id
        ));
        $rij = $stmt->fetch(PDO::FETCH_ASSOC);

        if (!$rij) {
            return null;
        } else {
            $item = Geldlade::create(
                $rij["id"],
                $rij["munt"],
                $rij['aantal']
            );
            $dbh = null;
            return $item;
        }
    }

    public function add($id, $aantal)
    {
        $sql =
            "UPDATE aldofi1q_opleiding_frisdrank.geldlade
            SET aantal = aantal + :aantal
            WHERE id = :id";
        $dbh = new PDO(
            DBconfig::$DB_CONNSTRING,
            DBconfig::$DB_USERNAME,
            DBconfig::$DB_PASSWORD
        );
        $stmt = $dbh->prepare($sql);
        $stmt->execute(array(':id' => $id, ':aantal' => $aantal));

        // $id = $dbh->lastInsertId();
        $dbh = null;

        // Haal de munt op welke je gooide
        $item = self::getById($id);
        return $item;
    }
    public function remove($id, $aantal)
    {
        $sql =
            "UPDATE aldofi1q_opleiding_frisdrank.geldlade
            SET aantal = aantal - :aantal
            WHERE id = :id";
        $dbh = new PDO(
            DBconfig::$DB_CONNSTRING,
            DBconfig::$DB_USERNAME,
            DBconfig::$DB_PASSWORD
        );
        $stmt = $dbh->prepare($sql);
        $stmt->execute(array(':id' => $id, ':aantal' => $aantal));

        // $id = $dbh->lastInsertId();
        $dbh = null;

        // Haal de munt op welke je gooide
        // $item = self::getById($id);
        // return $item;
    }
    public function set($id, $aantal)
    {
        $sql =
            "UPDATE aldofi1q_opleiding_frisdrank.geldlade
            SET aantal = :aantal
            WHERE id = :id";
        $dbh = new PDO(
            DBconfig::$DB_CONNSTRING,
            DBconfig::$DB_USERNAME,
            DBconfig::$DB_PASSWORD
        );
        $stmt = $dbh->prepare($sql);
        $stmt->execute(array(
            ':id' => $id,
            ':aantal' => $aantal
        ));

        // $id = $dbh->lastInsertId();
        $dbh = null;

        // Haal nieuw aantal op
        $item = self::getById($id);
        return $item;
    }
}