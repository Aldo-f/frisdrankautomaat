<?php
namespace Frisdrank\Data;

use PDO;
use Frisdrank\Data\DBconfig;
use Frisdrank\Entities\Frisdrank;

class FrisdrankDAO
{
    public function getAll()
    {
        $sql =
            "SELECT * 
            FROM aldofi1q_opleiding_frisdrank.frisdrank;";
        $dbh = new PDO(
            DBconfig::$DB_CONNSTRING,
            DBconfig::$DB_USERNAME,
            DBconfig::$DB_PASSWORD
        );
        $resultSet = $dbh->query($sql);

        $lijst = array();
        foreach ($resultSet as $rij) {
            $item = Frisdrank::create(
                $rij["id"],
                $rij["naam"],
                $rij['aantal'],
                $rij['prijs']
            );
            array_push($lijst, $item);
        }
        $dbh = null;
        // echo $lijst;
        return $lijst;
    }

    public function getById($id)
    {
        $sql =
            "SELECT * 
            FROM aldofi1q_opleiding_frisdrank.frisdrank
            WHERE id = :id;";
        $dbh = new PDO(
            DBconfig::$DB_CONNSTRING,
            DBconfig::$DB_USERNAME,
            DBconfig::$DB_PASSWORD
        );

        $stmt = $dbh->prepare($sql);
        $stmt->execute(array(
            ':id' => $id
        ));
        $rij = $stmt->fetch(PDO::FETCH_ASSOC);

        if (!$rij) {
            return null;
        } else {
            $item = Frisdrank::create(
                $rij["id"],
                $rij["naam"],
                $rij['aantal'],
                $rij['prijs']
            );
            $dbh = null;
            return $item;
        }
    }

    public function remove($id, $aantal)
    {
        // controleer of de frisdank nog aanwezig is (extra controle)
        $item = self::getById($id);

        if ($item == null || $item->getAantal() < $aantal) {
            // het item is niet meer beschikbaar of er kunnen niet meer zoveel verwijderd worden
        } else {
            $sql =
                "UPDATE aldofi1q_opleiding_frisdrank.frisdrank
            SET aantal = aantal - :aantal
            WHERE id = :id;";
            $dbh = new PDO(
                DBconfig::$DB_CONNSTRING,
                DBconfig::$DB_USERNAME,
                DBconfig::$DB_PASSWORD
            );

            $stmt = $dbh->prepare($sql);
            $stmt->execute(array(
                ':id' => $id,
                ':aantal' => $aantal
            ));
            $stmt->fetch(PDO::FETCH_ASSOC);


            // De frisdrank is uit de DB
            $dbh = null;

            // toon de reeds eerder opgehaalde frisdrank
            return $item;
        }
    }

    public function set($aantal)
    {

        $sql =
            "UPDATE aldofi1q_opleiding_frisdrank.frisdrank
            SET aantal = :aantal";
        $dbh = new PDO(
            DBconfig::$DB_CONNSTRING,
            DBconfig::$DB_USERNAME,
            DBconfig::$DB_PASSWORD
        );

        $stmt = $dbh->prepare($sql);
        $stmt->execute(array(
            ':aantal' => $aantal
        ));
        $stmt->fetch(PDO::FETCH_ASSOC);


        // De frisdrank is uit de DB
        $dbh = null;
    }
}
