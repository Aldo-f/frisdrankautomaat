<?php
namespace Frisdrank\Entities;

class Geldlade
{
    private static $idMap = array();

    protected $id;
    protected $munt;
    protected $aantal;

    public function __construct($id, $munt, $aantal)
    {
        $this->id = $id;
        $this->munt = $munt;
        $this->aantal = $aantal;
    }

    public static function create($id, $munt, $aantal)
    {
        if (!isset(self::$idMap[$id])) {
            self::$idMap[$id] = new Geldlade(
                $id,
                $munt,
                $aantal,
            );
        }
        return self::$idMap[$id];
    }

    /**
     * Get the value of id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of munt
     */
    public function getMunt()
    {
        return $this->munt;
    }

    /**
     * Set the value of munt
     *
     * @return  self
     */
    public function setMunt($munt)
    {
        $this->munt = $munt;

        return $this;
    }

    /**
     * Get the value of aantal
     */
    public function getAantal()
    {
        return $this->aantal;
    }

    /**
     * Set the value of aantal
     *
     * @return  self
     */
    public function setAantal($aantal)
    {
        $this->aantal = $aantal;

        return $this;
    }
}