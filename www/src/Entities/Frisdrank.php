<?php
namespace Frisdrank\Entities;

class Frisdrank
{
    private static $idMap = array();

    protected $id;
    protected $naam;
    protected $aantal;
    protected $prijs;

    public function __construct($id, $naam, $aantal, $prijs)
    {
        $this->id = $id;
        $this->naam = $naam;
        $this->aantal = $aantal;
        $this->prijs = $prijs;
    }

    public static function create($id, $naam, $aantal, $prijs)
    {
        if (!isset(self::$idMap[$id])) {
            self::$idMap[$id] = new Frisdrank(
                $id,
                $naam,
                $aantal,
                $prijs
            );
        }
        return self::$idMap[$id];
    }

    /**
     * Get the value of id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of naam
     */
    public function getNaam()
    {
        return $this->naam;
    }

    /**
     * Set the value of naam
     *
     * @return  self
     */
    public function setNaam($naam)
    {
        $this->naam = $naam;

        return $this;
    }

    /**
     * Get the value of aantal
     */
    public function getAantal()
    {
        return $this->aantal;
    }

    /**
     * Set the value of aantal
     *
     * @return  self
     */
    public function setAantal($aantal)
    {
        $this->aantal = $aantal;

        return $this;
    }

    /**
     * Get the value of prijs
     */
    public function getPrijs()
    {
        return $this->prijs;
    }

    /**
     * Set the value of prijs
     *
     * @return  self
     */
    public function setPrijs($prijs)
    {
        $this->prijs = $prijs;

        return $this;
    }
}