<?php
namespace Frisdrank\Entities;


class Gebruiker
{
    private static $idMap = array();

    protected $id;
    protected $naam;
    protected $wachtwoord;

    public function __construct($id, $naam, $wachtwoord)
    {
        $this->id = $id;
        $this->naam = $naam;
        $this->aantal = $wachtwoord;
    }

    public static function create($id, $naam, $wachtwoord)
    {
        if (!isset(self::$idMap[$id])) {
            self::$idMap[$id] = new Gebruiker(
                $id,
                $naam,
                $wachtwoord
            );
        }
        return self::$idMap[$id];
    }

    /**
     * Get the value of id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of naam
     */
    public function getNaam()
    {
        return $this->naam;
    }

    /**
     * Set the value of naam
     *
     * @return  self
     */
    public function setNaam($naam)
    {
        $this->naam = $naam;

        return $this;
    }

    /**
     * Get the value of wachtwoord
     */
    public function getWachtwoord()
    {
        return $this->wachtwoord;
    }

    /**
     * Set the value of wachtwoord
     *
     * @return  self
     */
    public function setWachtwoord($wachtwoord)
    {
        $this->wachtwoord = $wachtwoord;

        return $this;
    }
}