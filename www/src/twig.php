<?php
require_once "vendor/autoload.php";

$loader = new \Twig\Loader\FilesystemLoader('src/presentation');
$twig = new \Twig\Environment(
    $loader,
    [
        'debug' => false
    ]
);

$twig->addExtension(new \Twig\Extension\DebugExtension());