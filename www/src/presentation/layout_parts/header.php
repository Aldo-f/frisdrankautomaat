<!doctype html>
<html lang="nl-be">

<head>
    <title>Frisdrankautomaat</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"> -->
    <!-- fomantic-ui -->
    <!-- <link rel="stylesheet" href="src/assets/min.css/semantic.css"> -->
    <!-- cdn semantic-ui -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/semantic-ui@2.4.2/dist/semantic.min.css">
    <!-- Font Awesome -->
    <!-- <link rel="stylesheet" href="src/assets/min.css/all.css"> -->
    <link rel="stylesheet" href="src/assets/min.css/style.css"> <!-- Eigen css -->
</head>

<body>
    <?php require "nav.php"; ?>
    <div class="ui main container">