<div class="ui top fixed inverted menu">
    <div class="ui container">
        <a class="header item" href="index.php">
            <img class="ui mini image" src="src/assets/img/dflogo.png" />
            Dizzy Frinks
        </a>
        <a class="item" href="index.php">Home</a>
        <div class="ui simple dropdown item">
            Admin menu<i class="dropdown icon"></i>
            <div class="menu">

                <?php
                if (isset($ingelogd) && $ingelogd == false) {
                    ?>
                <a class="item" href="login.php">Login</a>
                <?php
            } else {
                ?>
                <div class="devider"></div>
                <a class="item" href="?actie=logout">Logout</a>
                <a class="item" href="overzicht.php">Overzicht</a>
                <?php } ?>

            </div>
        </div>
    </div>
</div>