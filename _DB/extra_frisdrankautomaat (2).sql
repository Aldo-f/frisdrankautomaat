-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 05, 2019 at 09:54 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 7.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `extra_frisdrankautomaat`
--

-- --------------------------------------------------------

--
-- Table structure for table `frisdrank`
--

CREATE TABLE `frisdrank` (
  `id` int(11) NOT NULL,
  `naam` varchar(45) NOT NULL,
  `aantal` int(11) NOT NULL,
  `prijs` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `frisdrank`
--

INSERT INTO `frisdrank` (`id`, `naam`, `aantal`, `prijs`) VALUES
(1, 'Coca-Cola', 18, '0.60'),
(2, 'Fanta', 20, '0.60'),
(3, 'Ice Tea', 20, '0.80'),
(4, 'Fruitsap', 20, '0.50'),
(5, 'Chaudfontaine', 20, '0.50');

-- --------------------------------------------------------

--
-- Table structure for table `gebruiker`
--

CREATE TABLE `gebruiker` (
  `id` int(11) NOT NULL,
  `naam` varchar(45) NOT NULL,
  `wachtwoord` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `gebruiker`
--

INSERT INTO `gebruiker` (`id`, `naam`, `wachtwoord`) VALUES
(1, 'admin', 'admin'),
(2, 'admin', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `geldlade`
--

CREATE TABLE `geldlade` (
  `id` int(11) NOT NULL,
  `munt` decimal(10,2) NOT NULL,
  `aantal` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `geldlade`
--

INSERT INTO `geldlade` (`id`, `munt`, `aantal`) VALUES
(1, '2.00', 10036),
(3, '1.00', 9964),
(4, '0.50', 987),
(5, '0.20', 497),
(6, '0.10', 500);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `frisdrank`
--
ALTER TABLE `frisdrank`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gebruiker`
--
ALTER TABLE `gebruiker`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `geldlade`
--
ALTER TABLE `geldlade`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `frisdrank`
--
ALTER TABLE `frisdrank`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `gebruiker`
--
ALTER TABLE `gebruiker`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `geldlade`
--
ALTER TABLE `geldlade`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
